# gesture_defined_robot_movements
SICK Solution World 2021 Hackathon

Team "G.I. Ro":
- Manuel von Hochmeister 
- Finn Jonas Peper
- Samuel Winterhalder
- Jariatou Jallow

## Challenge
Implement a robot system which is controlable with hand gestures
## Solution 

system components: 
- SICK InspectorP62x
- Universal Robot UR3e

![System overview](imgs/system_overview.png "system overview")
![hand gesture recognition](imgs/hand_gesture_recognition.png "hand gesture recognition")

## Possible Improvemtents
- Extend the functionality to a continious space 
- Allow objects to be arbitrary orientated
- Interepretate different gestures 