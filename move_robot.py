# Echo client program
import socket
import json
import numpy as np


class RobotMover:
    HOST = "172.16.8.125"  # The UR IP address
    PORT = 30002  # UR secondary client

    string_popup = "popup(\"hallo\")\n"

    string_header = "def function():\n"
    string_footer = "end\n"

    string_close = "set_tool_digital_out(0,False)\n set_tool_digital_out(1,True)\nsleep(.5)\n"
    string_open = "set_tool_digital_out(0,True)\n set_tool_digital_out(1,False)\nsleep(.5)\n"

    positions = [[.28, -.025],
                 [.28, -.105],
                 [.36, -.025],
                 [.36, -.105],
                 [.44, -.025],
                 [.44, -.105]]

    home = [.3, .3, .15, 3.14, 0, 0]

    string = ""

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.HOST, self.PORT))

    def move(self, x, y, z, alpha, beta, gamma):
        # move to a specific pose definied by 3 positional and 3 angular coordinates

        # prevent crashing into the vision
        assert z <= 0.15, "z is not allowed to be greater than 0.1"

        string_move = f"movel(p[{x},{y},{z},{alpha},{beta},{gamma}])\n"
        return string_move

    def move_specific(self, pos, high=True):
        # like move() but in a more task specific way: Takes the x and y coordinate of the desired
        # position pos = [x,y] and a boolean to define if the position should be in the low (z=0.4)
        # or the high (z= 0.15) position. The orientation of the toolcenterpoint ist everytime the same.

        if high:
            z = .15
        else:
            z = .04
        string = self.move(pos[0], pos[1], z, 3.14, 0.17, 0)
        return string

    def init_positions(self, a, b):
        # initial setup, takes to integers a and b (which are correpsonding to the defined self.positions)
        # and move the blocks from the starting points to the given position

        pos_part_1_start = [.224, .043]
        pos_part_1_target = self.positions[a]
        pos_part_2_start = [.224, .103]
        pos_part_2_target = self.positions[b]

        command_list = [self.string_header,
                        self.string_home(),
                        self.move_specific(pos_part_1_start, True),
                        self.move_specific(pos_part_1_start, False),
                        self.string_close,
                        self.move_specific(pos_part_1_start, True),
                        self.move_specific(pos_part_1_target, True),
                        self.move_specific(pos_part_1_target, False),
                        self.string_open,
                        self.move_specific(pos_part_1_target, True),
                        self.move_specific(pos_part_2_start, True),
                        self.move_specific(pos_part_2_start, False),
                        self.string_close,
                        self.move_specific(pos_part_2_start, True),
                        self.move_specific(pos_part_2_target, True),
                        self.move_specific(pos_part_2_target, False),
                        self.string_open,
                        self.move_specific(pos_part_2_target, True),
                        self.string_home(),
                        self.string_footer]

        string = self.concatenate_string(command_list)
        self.send_string(string)

    def string_home(self):
        # returns a string which could be send via tcp to open the gripper and
        # move the robot into the home position
        string = self.string_open
        string += self.move(self.positions[0],
                            self.positions[1],
                            self.positions[2],
                            self.positions[3],
                            self.positions[4],
                            self.positions[5])

        return string

    def homing(self):
        # homing the robot
        string = self.string_home()
        self.send_string(string)

    def move_block_from_a_2_b(self, a, b):
        # pick up a block at position a and put it at position b
        command_list = [self.string_header,
                        self.move_specific(self.positions[a], True),
                        self.move_specific(self.positions[a], False),
                        self.string_close,
                        self.move_specific(self.positions[a], True),
                        self.move_specific(self.positions[b], True),
                        self.move_specific(self.positions[b], False),
                        self.string_open,
                        self.move_specific(self.positions[b], True),
                        self.string_home(),
                        self.string_footer]

        string = self.concatenate_string(command_list)
        self.send_string(string)

    def wipe(self):
        # not used, "wipe" with the gripper to indicate a correctly identified gesture
        command_list = [
            self.move(.35, .1, .15, 3.14, 0, 0),
            self.move(.35, .1, .15, 3.14, 0, 1),
            self.move(.35, .1, .15, 3.14, 0, 0),
        ]
        string = self.concatenate_string(command_list)
        self.send_string(string)

    def concatenate_string(self, command_list):
        # takes a list of stringa and concatenate them to a stirng
        string = ""

        for command in command_list:
            string += command

        return string

    def send_string(self, string):
        # takes a string and send a ascii encoded string over the initialy created tcp socket
        self.s.sendall(string.encode("ascii"))

    def end(self):
        # close the socket connection
        self.s.close()
