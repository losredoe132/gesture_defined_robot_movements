import socket
import paho.mqtt.client as mqtt
import numpy as np
import time
import move_robot

# tcp stuff
HOST = '172.16.8.124'
PORT = 10000

# mqtt stuff
broker_url = "sickhack-mqtt-team2.germanywestcentral.azurecontainer.io"
broker_port = 1883

rm = move_robot.RobotMover()

n_equal_signals_needed = 3
n_remove_hand = 10


def process_message(data):
    # check if hand is detected
    datastr = data.decode('utf-8')
    msg = datastr.split(',')
    hand_detected = str(msg[0])
    distances = np.array(msg[1:]).astype(float)
    return hand_detected, distances


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print("Connected by", addr)

        pos_a_set = False
        pos_b_set = False
        pos_a = None
        pos_b = None
        counter_same_pos = 0
        remove_hand_counter = 10
        prev_pos = None

        while True:
            data = conn.recv(1024)
            if not data:
                break

            # print(data)
            hand_detected, distances = process_message(data)

            print(hand_detected)
            # print(distances)
            if hand_detected == 'true' and remove_hand_counter >= 10:
                pos_with_min_distance = distances.argmin()

                print(pos_with_min_distance)

                if pos_with_min_distance == prev_pos:
                    counter_same_pos += 1
                else:
                    counter_same_pos = 0

                if counter_same_pos >= n_equal_signals_needed and not pos_a_set:
                    pos_a = pos_with_min_distance
                    pos_a_set = True
                    counter_same_pos = 0
                    # rm.wipe()
                    # time.sleep(1)

                elif counter_same_pos >= n_equal_signals_needed and pos_a_set and not pos_b_set and pos_with_min_distance != pos_a:
                    pos_b = pos_with_min_distance
                    pos_b_set = True
                    counter_same_pos = 0
                    # rm.wipe()
                    # time.sleep(1)

                if pos_a_set and pos_b_set:
                    # send interpretation
                    print(
                        f"gesture interpretation: move block from {pos_a} to {pos_b}.")
                    rm.move_block_from_a_2_b(pos_a, pos_b)

                    # reinit positions
                    pos_a_set = False
                    pos_b_set = False

                    pos_a = None
                    pos_b = None

                    remove_hand_counter = 0

                prev_pos = pos_with_min_distance

                print(f'hand_detected = {hand_detected}')

                print(f'pos_a = {pos_a}, pos_b = {pos_b}')

            else:
                remove_hand_counter += 1
